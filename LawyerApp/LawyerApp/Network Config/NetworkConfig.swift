

import AFNetworking

class NetworkConfig: NSObject {
    
    
    static func getRequest( route : String,param: [String:Any], completion:@escaping (_ data : AnyObject?, _ error : Error?) -> Void)
    {
        //network configuration
        let manager = AFHTTPSessionManager(baseURL: URL(string: Routes.baseUrl))
        manager.requestSerializer.timeoutInterval = 10.0
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/json", "text/javascript","text/html", "application/x-www-form-urlencoded") as? Set<String>
        
        //get request
        manager.get(route, parameters: param, progress: nil, success: { (task, data) in
           
            //return completion with data
            completion(data as AnyObject?, nil)

        }) { (task, error) in
            //return completion with error
            completion(nil,error)
        }
        
        
    }
    
    
    static func PostRequest(token: String?, params : [String:String]?, route : String, completion:@escaping (_ data : AnyObject?,_ task: AnyObject?, _ error : Error?) -> Void)
    {
        let manager = AFHTTPSessionManager(baseURL: URL(string: Routes.baseUrl))
        manager.responseSerializer.acceptableStatusCodes = nil
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        //manager.requestSerializer.setValue(token, forHTTPHeaderField: "Authorization")
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/json", "text/javascript","text/html", "application/x-www-form-urlencoded", "text/plain", "multipart/form-data" ) as? Set<String>
        
        manager.post(route, parameters: params, progress: nil, success: { (task, response) in
            
            completion(response as AnyObject?, task,nil)
        })
        { (task, error) in
            completion(nil, task,error)
            print("show error:\(error):\(task)")
        }
        
    }
    
    
 
    static func properMultipartRequestForUpload(token : String? , fileData : [String: Any] , route: String, params : Any?, onSuccess:  @escaping (_ data:NSDictionary,_ task: AnyObject) -> (),  onFailure: @escaping( _ error : Error) ->Void){
        
        
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: Routes.baseUrl))
        manager.responseSerializer.acceptableStatusCodes = nil
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/html") as? Set<String>
        
//        if let token = token {
//            manager.requestSerializer.setValue(token, forHTTPHeaderField: "Authorization")
//        }
        
        manager.post(route, parameters: params, constructingBodyWith: { (formData) in
            for (key,value) in fileData{
                
                
                let name = key
                if let file = value as? Data{
                    if let isString = String(data: file , encoding: .utf8){
                        NSLog("ConvertingBackToString:\(isString)")
                        formData.appendPart(withForm: file , name: name)
                        
                    }else if UIImage(data: file) != nil{
                        formData.appendPart(withFileData: file, name: name, fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/jpg")
                        
                    }
                }else if let images = value as? NSArray{
                    print(name)
                    print(value)
                    var count = 0
                    for image in images{
                        
                        formData.appendPart(withFileData: image as! Data, name: "images[\(count)]", fileName: "image.png", mimeType: "image/jpg")
                        count+=1
                        
                        // formData.appendPart
                    }
                    formData.appendPart(withForm: "\(count)".data(using: .utf8)! , name: "count")
                }
            }
            
            
        }, progress: nil, success: { (task, data) in
            print("Image Upload :\(task):\(data)")
            if let object = data as? NSDictionary {
                onSuccess(object, task)
            }
        }) { (task, error) in
            print("Image Upload Error:\(task):\(error)")
            onFailure((error as NSError?)!)
        }
    }
    
    
    
    
    
    
}
