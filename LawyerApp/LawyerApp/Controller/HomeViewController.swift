//
//  ViewController.swift
//  LawyerApp
//
//  Created by Invision on 26/02/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit
import ImageSlideshow
import SwiftyJSON

class HomeViewController: UIViewController,signInprotocol{

    @IBOutlet weak var legalAdviceButton: UIButton!
    @IBOutlet weak var requestLawyerButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var imageSlide: ImageSlideshow!
    
    var caseCategoriesArray : [CaseCategories] = []
    var NewsArray : [News] = []
    var imagesArray : [String] = []
    
let authprotocol = AuthenticationViewController()
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //slider configuration
        let slidertap = UITapGestureRecognizer(target: self, action: #selector(slidertapped(_:)))
        imageSlide.addGestureRecognizer(slidertap)
        imageSlide.slideshowInterval = 3.0
        imageSlide.contentScaleMode = .scaleToFill;
     
    }
    

   //delegate but not working
    func onSuccess(success: Bool) {
        
        
        if success {
            
            self.tabcontrollerSetter()
        }
    }

   //getting user profile if exists
    func getProfile () {
        
        
        if  let Userdict = UserDefaults.standard.object(forKey: "usercredentials") as? Dictionary<String,String> {
            
            
            let params = ["KEY" : Routes.apiKey,"emailOrNatID" : Userdict["emailOrNatID"]!,"password":
                Userdict["password"]!]
            
            NetworkConfig.getRequest(route: Routes.signInUser, param: params) { (response, error) in
                if error == nil {
                    
                    let json = JSON(response as AnyObject)
                    
                    let userObj = User(json: json)
                    
                    if let result = userObj.result {
                        
                        if result == -1 {
                            
                            
                            // KVNProgress.showError(withStatus: "User Exists")
                        }
                        else if result == -2 {
                            
                            //KVNProgress.showError(withStatus: "SignIn failed Try again")
                            
                        }
                        else  {
                            
                            
                            
                            if let customer = userObj.customer {
                                
                                CurrentUser.customer = customer
                            }
                            
                            if let subscribeObj = userObj.customer?.subscription {
                                
                                CurrentUser.subscription = subscribeObj
                            }
                            
                            self.tabcontrollerSetter()
                            
                            
                        }
                    }
                    
                }
                else {
                    
                    
                }
                
                
            }
            
        }
   
        

      
    }
    

    func tabcontrollerSetter () {
        
        
        if let customerObj = CurrentUser.customer {
            
            logoutButton.isHidden = false
            
            
            
            if let _ = customerObj.subscription?.delegateId {
                
                var tabviewcontrollers = tabBarController?.viewControllers
                let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "subscriberVC")
                
                let vc1 =
                    UINavigationController(rootViewController:rootVC)
                 vc1.navigationController?.navigationBar.tintColor = Util.primaryColor
                tabviewcontrollers![2] =  vc1
                
                tabBarController?.setViewControllers(tabviewcontrollers, animated: false)
                tabBarController?.tabBar.items?[2].image = UIImage(named: "profile_icon.png")?.withRenderingMode(.alwaysTemplate)
                
            }
            else {
                
                var viewcontroed = tabBarController?.viewControllers
                let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "employeeVC")
                let vc1 =
                    UINavigationController(rootViewController:rootVC)
                vc1.navigationController?.navigationBar.tintColor = Util.primaryColor
                viewcontroed![2] =  vc1
                tabBarController?.setViewControllers(viewcontroed, animated: false)
                
                tabBarController?.tabBar.items?[2].image = UIImage(named: "profile_icon.png")?.withRenderingMode(.alwaysTemplate)
                
            }
            
            
            
        }
        else {
            
            logoutButton.isHidden = true

            var viewcontroed = tabBarController?.viewControllers
            let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sign up")
            
            let vc1 =
                UINavigationController(rootViewController:rootVC)

 vc1.navigationController?.navigationBar.tintColor = Util.primaryColor
            viewcontroed![2] =  vc1
            tabBarController?.setViewControllers(viewcontroed, animated: false)
            
            tabBarController?.tabBar.items?[2].image = UIImage(named: "profile_icon.png")?.withRenderingMode(.alwaysTemplate)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
   
        

        
        if UserDefaults.standard.object(forKey: "usercredentials") != nil {
            
            
            
            getProfile()
            
        }
        else {
            
            tabcontrollerSetter()
            
        }
        self.authprotocol.delegate = self

       
        if caseCategoriesArray.count <= 0 {
            
            getcategories()
        }
        
    }
    
    func getcategories () {
        
        let params = ["KEY":"\(Routes.apiKey)"]
        NetworkConfig.getRequest(route: Routes.categoriesNews, param: params) { (response, error) in
            if error == nil {
                
                
                let json = JSON(response as AnyObject)
                
                let cases = LawyerCases(json: json)
                
                self.NewsArray = cases.news!
            
                
                self.caseCategoriesArray = cases.caseCategories!
                
                if self.caseCategoriesArray.count > 0 {
                    
                self.legalAdviceButton.isUserInteractionEnabled = true
                self.requestLawyerButton.isUserInteractionEnabled = true
                    
                }
                

                var images : [SDWebImageSource] = []
                for singleNews in self.NewsArray {
                
                    
                    if let imageURLStrings = singleNews.imagePathAr {
                        
                        let imageurl = "\(Routes.imageUrl)\(imageURLStrings)"
                        let input = SDWebImageSource.init(urlString: imageurl)
                        
                        
                                images.append(input!)
                     
                        }
                    

                    
                    }
              
                self.imageSlide.setImageInputs(images)
                
                if  self.imageSlide.images.count > 0 {
                    
                    self.imageSlide.isUserInteractionEnabled = true
                }
                
            
            
                
                
           
             
                
         
            }
            
        }
        
        
    }
    
    
    
    @IBAction func logoutButtonTapped(sender: UIButton!) {
 
        let alertview = UIAlertController(title: "Exit", message: "Do you want to logout", preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            UserDefaults.standard.removeObject(forKey: "usercredentials")

            
            UserDefaults.standard.removeObject(forKey: "customerId")
            CurrentUser.customer = nil
            CurrentUser.subscription = nil
            
            self.tabcontrollerSetter()
        }
        let no = UIAlertAction(title: "No", style: .default) { (action) in
            
            
        }
        alertview.addAction(yes)
        alertview.addAction(no)
        present(alertview, animated: true, completion: nil)
      
    }
    

    
    @IBAction func switchLanguage(sender: UIBarButtonItem) {
//        
//        if L102Language.currentAppleLanguage() == "en" {
//            L102Language.setAppleLAnguageTo(lang: "ar")
//        } else {
//            L102Language.setAppleLAnguageTo(lang: "en")
//        }
    }
   

    @objc func slidertapped(_ sender: UITapGestureRecognizer) {
       
        
        performSegue(withIdentifier: "gotoNews", sender: nil)
    }
    
    @IBAction func legalAdviceTapped (sender : UIButton!) {
        
        performSegue(withIdentifier: "gotoLegalAdvice", sender: nil)

        }
    @IBAction func requestLawyerTapped (sender : UIButton!) {
        
        performSegue(withIdentifier: "gotoLawyer", sender: nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if   let newsVC = segue.destination as? LawNewsViewController {
            
                newsVC.news = self.NewsArray[imageSlide.currentPage]
            
                
        }
        
        
            if   let caseVC = segue.destination as? RequestLawyerViewController {
                
                caseVC.mainCategories = self.caseCategoriesArray

            }
            
            
            if  let caseVC = segue.destination as? LegalAdviceViewController {
                
                caseVC.mainCategories = self.caseCategoriesArray
        }
        
        
        
    }
}

