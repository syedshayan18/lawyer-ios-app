//
//  MapViewViewController.swift
//  LawyerApp
//
//  Created by Invision on 28/02/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import KVNProgress
class MapViewViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
var marker = GMSMarker()

    var locationArray : [Locations]?
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
    }
    
    
    
    func makeMakerfromLocations () {
     
        if let locArray = locationArray {
            for locArr in locArray {
                
                
                
                let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: Double(locArr.latValue!), longitude: Double(locArr.lonValue!))
                
            
                
                let marker1 = GMSMarker()
                
                
                marker1.position = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                marker1.icon = UIImage(named: "pin.png")
                marker1.title = locArr.nameAr
                
                marker1.map = self.mapView
                
            }
            
            if let locationArr = locationArray {
                
                let firstlatitude = locationArr[0].latValue!
                let firstLongtitude = locationArr[0].lonValue!
                
                let firstlocationcord:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: Double(firstlatitude), longitude: Double(firstLongtitude))
                
            
                
                let camera = GMSCameraPosition.camera(withLatitude: firstlocationcord.latitude, longitude:firstlocationcord.longitude, zoom: 9.0)
                
                
               
                
                let mapview = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
                    
               
                
                
                    self.mapView.animate(to: mapview.camera)
              
     
              
              
                
                
               
            }
           
            
            
            
      
        
        
        
        
        
        
       
            
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
              getLocations()
    }
    
    func getLocations () {
        
       // KVNProgress.show(withStatus: "Map is Loading...")
      
        KVNProgress.show()
        let params = ["KEY":"\(Routes.apiKey)"]
        NetworkConfig.getRequest(route: Routes.getLocations, param: params) { (response, error) in
            if error == nil {
                
                KVNProgress.showSuccess()
                
                    let json = JSON(response as AnyObject)
                
                let locationObj = Location(json: json)
                
                if locationObj.result == 0 {
                    
                    
                  self.locationArray =  locationObj.locations
                    
                    
                    self.makeMakerfromLocations()
                 
                    
                }
                else if locationObj.result == 2 {
                    
                    KVNProgress.showError(withStatus: "Failed to get locations, please check your Internet Connection")
                    
                }
               
                
            }
            else {
                
                KVNProgress.showError(withStatus: "Failed to get locations, please check your Internet Connection")
                
            }
            
            
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
