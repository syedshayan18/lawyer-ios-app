//
//  RequestLawyerViewController.swift
//  LawyerApp
//
//  Created by Invision on 28/02/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit
import KVNProgress
class RequestLawyerViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var idLabel: UILabel!
     @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var addAttachmentButton1: UIButton!
     @IBOutlet weak var addAttachmentButton2: UIButton!
     @IBOutlet weak var addAttachmentButton3: UIButton!
    @IBOutlet weak var removeAttachmentButton1: UIButton!
    @IBOutlet weak var removeAttachmentButton2: UIButton!
    @IBOutlet weak var removeAttachmentButton3: UIButton!
    
    @IBOutlet weak var explanationTextView: UITextView!
    @IBOutlet weak var subCategoryTextField: UITextField!
    @IBOutlet weak var mainCategoryTextField: UITextField!
    
    @IBOutlet weak var consultantDateLabel: UILabel!
        @IBOutlet weak var consultantNoLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    
    let mainCatpickerView = UIPickerView()
     let subCatpickerView = UIPickerView()
    let imagePicker = UIImagePickerController()
    
       var mainCategories : [CaseCategories]?
       var subCategories : [SubCategories]?
    var selectedIndex: Int = 0
    
    var subcategoryId : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userIsActive = CurrentUser.customer?.isActivated {
            
            if userIsActive {
                
                sendButton.isEnabled = true
                sendButton.backgroundColor = Util.primaryColor
                
                
            }
            else {
                    
                    sendButton.isEnabled = false
            }
            
        }
            
        else {
            
            sendButton.isEnabled = false
        }
            
        
    
        mainCategoryTextField.text! = mainCategories![0].nameAr!
 subCategoryTextField.text! = mainCategories![0].subCategories![0].nameAr!
        
    subcategoryId = mainCategories![0].subCategories![0].id!
        
        addAttachmentButton1.layer.borderColor = UIColor.black.cgColor
        addAttachmentButton1.layer.borderWidth = CGFloat(Util.borderWidth)
        addAttachmentButton1.layer.cornerRadius = 10
        
        addAttachmentButton2.layer.borderColor = UIColor.black.cgColor
        addAttachmentButton2.layer.borderWidth = CGFloat(Util.borderWidth)
        addAttachmentButton2.layer.cornerRadius = 10
        
        addAttachmentButton3.layer.borderColor = UIColor.black.cgColor
        addAttachmentButton3.layer.borderWidth = CGFloat(Util.borderWidth)
        addAttachmentButton3.layer.cornerRadius = 10
        
        
        mainCategoryTextField.inputView = mainCatpickerView
        subCategoryTextField.inputView = subCatpickerView
        
        mainCatpickerView.delegate = self
        mainCatpickerView.dataSource = self
        subCatpickerView.dataSource = self
        subCatpickerView.delegate = self
       
        
    }
    
    @IBAction func submitCasebutton () {
        
       submitCase()
    }
   
    func textfieldValidation () -> Bool {
        
        if !(mainCategoryTextField.text?.isEmpty)! && !(subCategoryTextField.text?.isEmpty)! && !(explanationTextView.text?.isEmpty)!{
            
            return true
            
        }
       
    return false
        
    }
    
    func submitCase () {
        
        if textfieldValidation() {
        
            if let customerId = CurrentUser.customer?.id {
                
                let params = ["KEY" : Routes.apiKey,"CustomerId" : customerId,"SubCategoryId":
                    subcategoryId!,"CaseTypeId" : 2 ,"Description" :"\(explanationTextView.text!)"] as [String : Any]
                
                NetworkConfig.getRequest(route: Routes.submitCase, param: params, completion: { (response, error) in
                    
                    
                    if error == nil {
                    
                        if let jsonResult = response as? Dictionary<String, AnyObject> {
                            
                            if let result = jsonResult["result"] as? Int {
                                if result == 0 {
                                    
                                    KVNProgress.showSuccess(withStatus: "Lawyer Requested Successfully")
//                                    self.navigationController?.popViewController(animated: true)
                                    
                                }
                                else if result == -1 {
                                    
                                    KVNProgress.showError(withStatus: " No customer exist with the given id")
                                }
                                else if result == -2 {
                                    
                                    KVNProgress.showError(withStatus: "Request failed please try again")
                                }
                                else if result == -3 {
                                    
                                    KVNProgress.showError(withStatus: "the customer account is not activated")
                                }
                                else {
                                    
                                     KVNProgress.showError(withStatus: "Error when creating the case record")
                                    
                                }
                                
                            }
                        }
                        
                        
                    }
                    else {
                        
                        KVNProgress.showError(withStatus: error?.localizedDescription)
                    }
                })
            }
 
            else {
                
                KVNProgress.showError(withStatus: "Please login for request a lawyer")
        }
            
        }
    
        else {
            
            KVNProgress.showError(withStatus: "Please fill all the fields")
        }
    }
    
    @IBAction func selectAttachmentTapped (sender: UIButton!)
    {
        self.showAlert(title: "Media", message: nil, action1: ("Open Gallery",{
            
            
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
            
        } ), action2: ("Open Camera",{
            
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.cameraCaptureMode = .photo
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }), action3: ("Cancel", {
            
            
            
        }))
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        //profileImageView.contentMode = .ScaleAspectFit
        //profileImageView.image = chosenImage
        
        
      

        dismiss(animated: true, completion: nil)
        
    }
    
 
    
   
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
}

extension RequestLawyerViewController {
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == mainCatpickerView {
            return (mainCategories?.count)!
        }
        else {
            
            
            
            return  (mainCategories![selectedIndex].subCategories?.count)!
            
            
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == mainCatpickerView {
            
            
            let mainCat = mainCategories![row] as CaseCategories
            return mainCat.nameAr
        }
        else {
            
            let subcat = mainCategories![selectedIndex].subCategories![row]
            return subcat.nameAr
            
        }
        
        
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == mainCatpickerView {
            
            
            let mainCat = mainCategories![row] as CaseCategories
            self.selectedIndex = row
            
            mainCategoryTextField.text! = mainCat.nameAr!
            
            subCategoryTextField.text! = mainCategories![row].subCategories![0].nameAr!
            
        }
        else {
            
            let subcat = mainCategories![selectedIndex].subCategories![row]
            
            subCategoryTextField.text! = subcat.nameAr!
            
            subcategoryId = subcat.id!
            
        }
        
    }
}

