//
//  EmployeeProfileViewController.swift
//  LawyerApp
//
//  Created by Invision on 09/03/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit

class EmployeeProfileViewController: UIViewController {
    @IBOutlet weak var employeeNoTextField: UITextField!
    @IBOutlet weak var supervisorNoTextField: UITextField!
    
    @IBOutlet weak var familyNameTextField: UITextField!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var emailNoTextField: UITextField!
    @IBOutlet weak var contactNoTextField: UITextField!
    @IBOutlet weak var receivedContractTextField: UITextField!
    
    @IBOutlet weak var passwordResetButton: UIButton!
    @IBOutlet weak var subscriberReqButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            navigationController?.navigationBar.barTintColor = Util.primaryColor
        self.navigationItem.title = "الملف الشخصي للموظف"
        navigationController?.navigationBar.tintColor = UIColor.white
        supervisorNoTextField.isUserInteractionEnabled = false
        employeeNoTextField.isUserInteractionEnabled = false
        
        familyNameTextField.text = CurrentUser.customer?.fatherName
        firstNameTextField.text = CurrentUser.customer?.firstName
        emailNoTextField.text = CurrentUser.customer?.email
        contactNoTextField.text = CurrentUser.customer?.mobile
        // Do any additional setup after loading the view.
    }
    
    
 
  
    @IBAction func logoutTapped (sender: UIButton!) {
        
        let alertview = UIAlertController(title: "Exit", message: "Do you want to logout", preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            UserDefaults.standard.removeObject(forKey: "usercredentials")
            
            CurrentUser.customer = nil
            CurrentUser.subscription = nil
            self.tabBarController?.selectedIndex = 0
            
        }
        let no = UIAlertAction(title: "No", style: .default) { (action) in
            
            
        }
        alertview.addAction(yes)
        alertview.addAction(no)
        present(alertview, animated: true, completion: nil)
  
    }

}
