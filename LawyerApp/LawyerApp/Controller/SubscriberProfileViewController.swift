//
//  SubscriberProfileViewController.swift
//  LawyerApp
//
//  Created by Invision on 09/03/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit

class SubscriberProfileViewController: UIViewController {
    @IBOutlet weak var contractNoTextField: UITextField!
     @IBOutlet weak var electronicSubTextField: UITextField!
     @IBOutlet weak var fatherNameTextField: UITextField!
     @IBOutlet weak var firstNameTextField: UITextField!
     @IBOutlet weak var familyNameTextField: UITextField!
     @IBOutlet weak var gNameTextField: UITextField!
     @IBOutlet weak var IDTextField: UITextField!
     @IBOutlet weak var nationalityTextField: UITextField!
     @IBOutlet weak var contactNumberTextField: UITextField!
     @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var issueNoTextField: UITextField!
    @IBOutlet weak var consultantNoTextField: UITextField!
    
    @IBOutlet weak var resetPasswordButton: UIButton!
    @IBOutlet weak var suggestnCompButton: UIButton!
    @IBOutlet weak var salesRequestButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = Util.primaryColor
        self.navigationItem.title = "الملف الشخصي للمشترك"
        navigationController?.navigationBar.tintColor = UIColor.white

        electronicSubTextField.isUserInteractionEnabled = false
        contractNoTextField.isUserInteractionEnabled = false
        
        if let _ = CurrentUser.customer?.subscription?.delegateId {
            
            
            firstNameTextField.text = CurrentUser.customer?.firstName
            fatherNameTextField.text = CurrentUser.customer?.fatherName
            gNameTextField.text = CurrentUser.customer?.grandName
            familyNameTextField.text = CurrentUser.customer?.surName
            contractNoTextField.text = CurrentUser.customer?.subscription?.contractNo
            nationalityTextField.text = "\(CurrentUser.customer!.nationalityCountryId!)"
            contactNumberTextField.text = CurrentUser.customer?.mobile
            emailTextField.text = CurrentUser.customer?.email
            
            
            electronicSubTextField.text = ("\(CurrentUser.customer!.subscription!.electronicContractNo!)")
            IDTextField.text = "\(CurrentUser.customer!.subscription!.delegateId!)"
            
            
        }
       
    }
    @IBAction func logoutTapped (sender: UIButton) {
        
        
        let alertview = UIAlertController(title: "Exit", message: "Do you want to logout", preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            UserDefaults.standard.removeObject(forKey: "usercredentials")
            
            CurrentUser.customer = nil
            CurrentUser.subscription = nil
            self.tabBarController?.selectedIndex = 0
            
        }
        let no = UIAlertAction(title: "No", style: .default) { (action) in
            
            
        }
        alertview.addAction(yes)
        alertview.addAction(no)
        present(alertview, animated: true, completion: nil)
        

    }

}
