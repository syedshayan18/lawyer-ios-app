//
//  LawNewsViewController.swift
//  LawyerApp
//
//  Created by Invision on 28/02/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit
import SDWebImage

class LawNewsViewController: UIViewController,UITextViewDelegate {
    @IBOutlet weak var explainTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    var news : News?
    var newsImage: UIImage?
    @IBOutlet weak var newsImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if let newsObj = news {
            
            let imageurl = "\(Routes.imageUrl)\(newsObj.imagePathAr!)"
            
            newsImageView.sd_setImage(with: URL(string:imageurl ), placeholderImage: UIImage(named: "request_lawyer_bg_01"))

            titleLabel.text = newsObj.titleAr
            explainTextView.text = newsObj.textAr
            
            
        }
        newsImageView.layer.cornerRadius = 10
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
