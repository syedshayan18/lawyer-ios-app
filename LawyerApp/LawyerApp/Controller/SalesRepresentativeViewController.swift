//
//  SalesRepresentativeViewController.swift
//  LawyerApp
//
//  Created by Invision on 03/03/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit
import KVNProgress
import SwiftyJSON
class SalesRepresentativeViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    @IBOutlet weak var firstNameTextfield: UITextField!
     @IBOutlet weak var fatherNameTextfield: UITextField!
     @IBOutlet weak var grandNameTextfield: UITextField!
     @IBOutlet weak var familyNameTextfield: UITextField!
     @IBOutlet weak var mobileTextfield: UITextField!
      @IBOutlet weak var citiesTextfield: UITextField!
      @IBOutlet weak var dateTextfield: UITextField!
    
    var citiesArray : [Cities] = []
    let pickerView = UIPickerView()
    let datePicker = UIDatePicker()
    var date : Data?
    var citiesId : [Int] = []
    var cityId : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        pickerView.delegate = self
        citiesTextfield.inputView = pickerView
        dateTextfield.inputView = datePicker
        
       showDatePicker()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getCities()
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateTextfield.inputAccessoryView = toolbar
        dateTextfield.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateTextfield.text = formatter.string(from: datePicker.date)
        
        //self.date = formatter.date(from: dateTextfield.text!)

      
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return citiesArray.count
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let city = citiesArray[row] as Cities
        
       
        return city.nameAr
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let city = citiesArray[row] as Cities
        self.cityId = city.id
       citiesTextfield.text = city.nameAr
    }
    
    
    
    func getCities(){
        
        
        
        NetworkConfig.getRequest(route: Routes.getCities, param:["KEY":Routes.apiKey] , completion: { (response, error) in
            
            if error == nil {
                
                
                KVNProgress.showSuccess()
                
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    
                    if let result = jsonResult["result"] as? Int {
                        
                        if result == 0 {
        
                            let citiesDict = jsonResult["cities"] as! Array<Dictionary<String, AnyObject>>
                        
                            
                            for item in citiesDict {
                                
                                let cityModel = Cities(object: item)
                                
                                self.citiesArray.append(cityModel)
                            }
                           
                      self.citiesTextfield.text =  self.citiesArray[0].nameAr
                            
                            self.cityId = self.citiesArray[0].id
                           
                             self.pickerView.reloadAllComponents()
                        }
                        else if result == -2 {
                      
                            
                        }
                    }
                    
                }
        
            }
            else {
                
                KVNProgress.showError(withStatus: error?.localizedDescription)
            }
        })
    }
    
    func textfieldValidation () -> Bool {
        
        if !(firstNameTextfield.text?.isEmpty)! && !(fatherNameTextfield.text?.isEmpty)! && !(grandNameTextfield.text?.isEmpty)! && !(familyNameTextfield.text?.isEmpty)! && !(mobileTextfield.text?.isEmpty)! && !(citiesTextfield.text?.isEmpty)! && !(dateTextfield.text?.isEmpty)! {
            
            return true
            
        }
        else {
            return false
        }
        
    }
    
    @IBAction func sendButtonTapped (sender : UIButton!) {
        
        if textfieldValidation() {
        
           
            
            if let cityID = cityId {
                
               
                let params = ["KEY" : Routes.apiKey,"CustomerId" : CurrentUser.customer!.id!,"FirstName":
                    "\(firstNameTextfield.text!)","FatherName" :"\(fatherNameTextfield.text!)" ,"GrandName" :"\(grandNameTextfield.text!)","SurName":
                    "\(familyNameTextfield.text!)","Mobile":"\(mobileTextfield.text!)","CityId":cityID,"date":"\(dateTextfield.text!)"] as [String : Any]
                print(params)
                
                 KVNProgress.show(withStatus: "Processing...")
                NetworkConfig.getRequest(route: Routes.requestDelegate, param: params , completion: { (response, error) in
                    
                    if error == nil {
                        
//                        (result=0): no error | (result=-1): customer is not exist | (result=-2): error occured. | (result=-3): has old delegate request . |
                        
                        if let jsonResult = response as? Dictionary<String, AnyObject> {
                            
                            if let result = jsonResult["result"] as? Int {
                                if result == 0 {
                                    
                                      KVNProgress.showSuccess(withStatus: "Delegate Successfully Registered!")
                                    
                                    self.performSegue(withIdentifier: "gotoSubscribtion", sender: nil)
                                }
                                else if result == -1 {
                                    
                                    KVNProgress.showError(withStatus: "Customer is not exist")
                                }
                                else if result == -2 {
                                    
                                    KVNProgress.showError(withStatus: "delegate Request failed, please try again")
                                }
                                else if result == -3 {
                                    
                                    KVNProgress.showError(withStatus: "Requested delegate is old delegate request")
                                }
                                
                            }
                        }
                        
                        
                    }
                    else {
                        
                        KVNProgress.showError(withStatus: error?.localizedDescription)
                    }
                })
                
            }
           
            
            }
        
        else {
            
            KVNProgress.showError(withStatus: "Please fill all the fields")
        }
          
           
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
