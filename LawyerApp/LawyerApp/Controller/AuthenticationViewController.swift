
//
//  ProfileViewController.swift
//  LawyerApp
//
//  Created by Invision on 28/02/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import UIKit
import SwiftyJSON
import KVNProgress

protocol signInprotocol:class {
    
    func onSuccess(success:Bool)
    
    
}
class AuthenticationViewController: UIViewController {
    
    weak var delegate:signInprotocol?

    @IBOutlet weak var personalInfoTextfield: UITextField!
    @IBOutlet weak var profileEmailTextfield: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var issueNoTextfield: UITextField!
    @IBOutlet weak var suggestionnCompTextfield: UITextField!
    @IBOutlet weak var contactNumTextfield: UITextField!
    @IBOutlet weak var consultantNumTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signUpViewBtn: UIButton!
    @IBOutlet weak var signInViewBtn: UIButton!
    
    @IBOutlet weak var confirmPassTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var passwordSignInTextField: UITextField!
    @IBOutlet weak var mobileNumTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
 
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var rememberMeSignIn: UIButton!
       @IBOutlet weak var remembermesignUp: UIButton!
    @IBOutlet weak var signInView: UIView!
    @IBOutlet weak var signUpView: UIView!
    
    @IBOutlet weak var profileView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = Util.primaryColor
        self.navigationItem.title = "تسجيل الدخول"
//
//        emailTextField.text = "S1"
//        mobileNumTextField.text = "123456"
//        passwordTextField.text = "123456"
//        confirmPassTextField.text = "123456"
//        
//        userNameTextField.text = "S1"
//        passwordSignInTextField.text = "123456"
        
        
        remembermesignUp.isSelected = false
        rememberMeSignIn.isSelected = false
        
        signInViewBtn.layer.backgroundColor = Util.secondaryColor.cgColor
        signUpViewBtn.layer.backgroundColor = Util.primaryColor.cgColor
        
        
        rememberMeSignIn.layer.borderColor = Util.primaryColor.cgColor
        rememberMeSignIn.layer.cornerRadius = 5
        rememberMeSignIn.layer.borderWidth = Util.checkbtnborderWidth
        
       remembermesignUp.layer.borderColor = Util.primaryColor.cgColor
        remembermesignUp.layer.borderWidth = Util.checkbtnborderWidth
        remembermesignUp.layer.cornerRadius = 5;
        
       
   
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.object(forKey: "usercredentials") != nil {

        }
        
        if let customerObj = CurrentUser.customer {
            
        
            
            
            if let _ = customerObj.subscription?.delegateId {
                
           
                
                
            }
            else {
                
           
                
            }
            
          
           // showprofileView()
            
        }
        else {
            
            showSignInView()

        }
       
        
            //let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            
//            let profileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profileVC") as! ProfileViewController
//            
//            tabBarController?.viewControllers?.remove(at: 2)
//
//            tabBarController?.viewControllers?.insert(profileVC, at: 2)
           //tabBarController?.viewControllers?.removeAll()
         
           // tabBarController?.setViewControllers(vc, animated: true)
            
            //for testing comment
        
        
    

    }
    
}



 //MARK: checkbox tapped functions
extension AuthenticationViewController {
    
   
    
    @IBAction func remembermeSignInTapped (sender : UIButton!) {
        
        if rememberMeSignIn.isSelected == true {
            rememberMeSignIn.isSelected =   false
            rememberMeSignIn.backgroundColor = UIColor.white
            
            
            
        }
        else {
            rememberMeSignIn.isSelected = true
            rememberMeSignIn.backgroundColor = Util.primaryColor
            
            
        }
    }
    
    
    
    @IBAction func remembermeSignUpTapped (sender : UIButton!) {
        
        if remembermesignUp.isSelected == true {
            remembermesignUp.isSelected =   false
            remembermesignUp.backgroundColor = UIColor.white
            
            
            
        }
        else {
            remembermesignUp.isSelected = true
            remembermesignUp.backgroundColor = Util.primaryColor
            
            
        }
        
    }
}

//MARK: Authentication views switched  functions

extension AuthenticationViewController {

    func showSignInView () {
           self.navigationItem.title = "تسجيل الدخول"
        signInViewBtn.backgroundColor = Util.secondaryColor
        signUpViewBtn.backgroundColor = Util.primaryColor
        signInView.isHidden = false
        signUpView.isHidden = true
        stackView.isHidden = false;
        profileView.isHidden = true

    }
    
    func showSignUpView () {
        
        signInViewBtn.backgroundColor = Util.primaryColor
        signUpViewBtn.backgroundColor = Util.secondaryColor
        signUpView.isHidden = false
        signInView.isHidden = true
        stackView.isHidden = false;
        profileView.isHidden = true
    }
    
   func showprofileView () {
    
    profileView.isHidden = false
    signUpView.isHidden = true
    signInView.isHidden = true
    stackView.isHidden = true;
    self.navigationItem.title = "الملف الشخصي"

    
    profileEmailTextfield.text! = (CurrentUser.customer?.email!)!
    contactNumTextfield.text! = (CurrentUser.customer?.mobile!)!
   
    }
    
    @IBAction func signInViewBtnTapped (sender : UIButton!) {
        
       showSignInView()
        
    }
    
    @IBAction func signUpViewBtnTapped (sender : UIButton!) {
        
      
        showSignUpView()
        
        
    }
}


//Server Hit authentication functions
extension AuthenticationViewController {
    
    @IBAction func signInbuttonTapped (sender : UIButton!) {

        if !(userNameTextField.text?.isEmpty)! && !(passwordSignInTextField.text?.isEmpty)! {
            
            KVNProgress.show(withStatus: "Signing in...")
          
            
            let params = ["KEY" : Routes.apiKey,"emailOrNatID" : "\(userNameTextField.text!)","password":
                "\(passwordSignInTextField.text!)"]
            
            
            NetworkConfig.getRequest(route: Routes.signInUser, param: params) { (response, error) in
                if error == nil {
                    
                    KVNProgress.showSuccess()
                    let json = JSON(response as AnyObject)
                    
                    let userObj = User(json: json)
                    
                    if let result = userObj.result {
                        
                        if result == -1 {
                            
                            KVNProgress.showError(withStatus: "wrong username or password")
                        }
                        else if result == -2 {
                            
                            KVNProgress.showError(withStatus: "SignIn failed Try again")
                            
                        }
                        else  {
                            
                            
                            
                            
                            if self.rememberMeSignIn.isSelected {
                                
                                let userCredentialsDict = ["emailOrNatID":self.emailTextField.text!,"password":self.passwordTextField.text!]
                                UserDefaults.standard.set(userCredentialsDict, forKey: "usercredentials")
                            }
                            
                            
                            if let customer = userObj.customer {
                                
                                CurrentUser.customer = customer
                            }
                            
                            if let subscribeObj = userObj.customer?.subscription {
                                
                                CurrentUser.subscription = subscribeObj
                            }
                            
                            self.tabBarController?.selectedIndex = 0
                            
                            self.delegate?.onSuccess(success: true)
                            
                        }
                    }
                    
                }
                else {
                    
                    KVNProgress.showError(withStatus: error?.localizedDescription)
                    
                }
                
                
            }
            
           
        }
        else {
            
                KVNProgress.showError(withStatus: "Please fill all fields")
        }
        
     
    }
    
    
    
    
    @IBAction func signUpbuttonTapped (sender : UIButton!) {
        
        if !(emailTextField.text?.isEmpty)! && !(mobileNumTextField.text?.isEmpty)! && !(passwordTextField.text?.isEmpty)! && !(confirmPassTextField.text?.isEmpty)! {
            
            KVNProgress.show(withStatus: "Registering...")
            let params = ["KEY":"\(Routes.apiKey)","email":"\(emailTextField.text!)","mobileNo":"\(mobileNumTextField.text!)","password":"\(passwordTextField.text!)"]
            NetworkConfig.getRequest(route: Routes.registerUser, param: params) { (response, error) in
                if error == nil {
                    
                    if let jsonResult = response as? Dictionary<String, AnyObject> {
                        
                        if let name = jsonResult["result"] as? Int {
                            print(jsonResult)
                            if name == 0 {
                                
                                
                                KVNProgress.showError(withStatus: "User Created Succesfully")
                                self.showSignInView()
                                
                            }
                            else if name == -1 {
                                
                                KVNProgress.showError(withStatus: "User with this name or email exits")
                               
                                
                            }
                            else {
                                    KVNProgress.showError(withStatus: "Signup failed Try again")
                             
                                
                            }
                            
                            
                        }
                    }
                    
                }
                else {
                    
                     KVNProgress.showError(withStatus: error?.localizedDescription)
                    
                    
                    
                }
                
                
            }
        }
        
        else {
            
              KVNProgress.showError(withStatus: "Please fill all fields")
           
        }
            
        }
   
        
}

//profilework
extension AuthenticationViewController  {
    
    
  
    
    @IBAction func backButtonTapped ( sender:UIButton!) {
        

        
        tabBarController?.selectedIndex = 0
     
        
      
    
        

    }
    
    @IBAction func gotoSalesButtonTapped ( sender:UIButton!) {
        self.showAlert(title: "Do you have sales representative Number?", message: nil, action1: ("No",{
            
            self.performSegue(withIdentifier: "gotoSales", sender: nil)

            
        } ), action2: ("Yes",{
           
             self.performSegue(withIdentifier: "gotoSubscribtion", sender: nil)
            
        }), action3: ("Cancel", {
            
            
        }))
    }
}
