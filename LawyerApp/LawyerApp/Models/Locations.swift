//
//  Locations.swift
//
//  Created by Invision on 06/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class Locations {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isDeleted = "IsDeleted"
    static let nameAr = "NameAr"
    static let lonValue = "LonValue"
    static let latValue = "LatValue"
    static let id = "Id"
  }

  // MARK: Properties
  public var isDeleted: Bool? = false
  public var nameAr: String?
  public var lonValue: Float?
  public var latValue: Float?
  public var id: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    isDeleted = json[SerializationKeys.isDeleted].boolValue
    nameAr = json[SerializationKeys.nameAr].string
    lonValue = json[SerializationKeys.lonValue].float
    latValue = json[SerializationKeys.latValue].float
    id = json[SerializationKeys.id].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.isDeleted] = isDeleted
    if let value = nameAr { dictionary[SerializationKeys.nameAr] = value }
    if let value = lonValue { dictionary[SerializationKeys.lonValue] = value }
    if let value = latValue { dictionary[SerializationKeys.latValue] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    return dictionary
  }

}
