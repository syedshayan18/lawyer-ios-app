//
//  Cities.swift
//
//  Created by Invision on 04/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Cities: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let nameAr = "NameAr"
    static let nameEn = "NameEn"
    static let id = "Id"
  }

  // MARK: Properties
  public var nameAr: String?
  public var nameEn: String?
  public var id: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    nameAr = json[SerializationKeys.nameAr].string
    nameEn = json[SerializationKeys.nameEn].string
    id = json[SerializationKeys.id].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = nameAr { dictionary[SerializationKeys.nameAr] = value }
    if let value = nameEn { dictionary[SerializationKeys.nameEn] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.nameAr = aDecoder.decodeObject(forKey: SerializationKeys.nameAr) as? String
    self.nameEn = aDecoder.decodeObject(forKey: SerializationKeys.nameEn) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(nameAr, forKey: SerializationKeys.nameAr)
    aCoder.encode(nameEn, forKey: SerializationKeys.nameEn)
    aCoder.encode(id, forKey: SerializationKeys.id)
  }

}
