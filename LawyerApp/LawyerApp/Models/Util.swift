//
//  Util.swift
//  LawyerApp
//
//  Created by Invision on 28/02/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import Foundation
import UIKit
class Util {
    
    
  static  let primaryColor = UIColor.init(red:172/255.0 , green: 151/255.0, blue: 78/255.0, alpha: 1.0)
   static let secondaryColor = UIColor.init(red:142/255.0 , green: 126/255.0, blue: 52/255.0, alpha: 1.0)
    
    static let borderWidth = 1.0
    static let checkbtnborderWidth = CGFloat(5.0)
}
