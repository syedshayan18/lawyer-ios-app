//
//  LawyerCases.swift
//
//  Created by Invision on 05/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class LawyerCases {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let appRootPath = "AppRootPath"
    static let caseCategories = "caseCategories"
    static let news = "news"
    static let result = "result"
    static let homeCategories = "homeCategories"
  }

  // MARK: Properties
  public var appRootPath: String?
  public var caseCategories: [CaseCategories]?
  public var news: [News]?
  public var result: Int?
  public var homeCategories: [HomeCategories]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    appRootPath = json[SerializationKeys.appRootPath].string
    if let items = json[SerializationKeys.caseCategories].array { caseCategories = items.map { CaseCategories(json: $0) } }
    if let items = json[SerializationKeys.news].array { news = items.map { News(json: $0) } }
    result = json[SerializationKeys.result].int
    if let items = json[SerializationKeys.homeCategories].array { homeCategories = items.map { HomeCategories(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = appRootPath { dictionary[SerializationKeys.appRootPath] = value }
    if let value = caseCategories { dictionary[SerializationKeys.caseCategories] = value.map { $0.dictionaryRepresentation() } }
    if let value = news { dictionary[SerializationKeys.news] = value.map { $0.dictionaryRepresentation() } }
    if let value = result { dictionary[SerializationKeys.result] = value }
    if let value = homeCategories { dictionary[SerializationKeys.homeCategories] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
