//
//  Customer.swift
//
//  Created by Invision on 09/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Customer: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let noLegalAdvices = "NoLegalAdvices"
    static let email = "Email"
    static let nationalityCountryNameAr = "NationalityCountryNameAr"
    static let gender = "Gender"
    static let fatherName = "FatherName"
    static let grandName = "GrandName"
    static let nationalityCountryId = "NationalityCountryId"
    static let natIdExpDate = "NatIdExpDate"
    static let mobile = "Mobile"
    static let lastDelegateRequestId = "LastDelegateRequestId"
    static let noLawyerRequests = "NoLawyerRequests"
    static let firstName = "FirstName"
    static let id = "Id"
    static let nationalityCountryNameEn = "NationalityCountryNameEn"
    static let isActivated = "IsActivated"
    static let subscription = "Subscription"
    static let surName = "SurName"
    static let natId = "NatId"
  }

  // MARK: Properties
  public var noLegalAdvices: Int?
  public var email: String?
  public var nationalityCountryNameAr: String?
  public var gender: String?
  public var fatherName: String?
  public var grandName: String?
  public var nationalityCountryId: Int?
  public var natIdExpDate: String?
  public var mobile: String?
  public var lastDelegateRequestId: Int?
  public var noLawyerRequests: Int?
  public var firstName: String?
  public var id: Int?
  public var nationalityCountryNameEn: String?
  public var isActivated: Bool? = false
  public var subscription: Subscription?
  public var surName: String?
  public var natId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    noLegalAdvices = json[SerializationKeys.noLegalAdvices].int
    email = json[SerializationKeys.email].string
    nationalityCountryNameAr = json[SerializationKeys.nationalityCountryNameAr].string
    gender = json[SerializationKeys.gender].string
    fatherName = json[SerializationKeys.fatherName].string
    grandName = json[SerializationKeys.grandName].string
    nationalityCountryId = json[SerializationKeys.nationalityCountryId].int
    natIdExpDate = json[SerializationKeys.natIdExpDate].string
    mobile = json[SerializationKeys.mobile].string
    lastDelegateRequestId = json[SerializationKeys.lastDelegateRequestId].int
    noLawyerRequests = json[SerializationKeys.noLawyerRequests].int
    firstName = json[SerializationKeys.firstName].string
    id = json[SerializationKeys.id].int
    nationalityCountryNameEn = json[SerializationKeys.nationalityCountryNameEn].string
    isActivated = json[SerializationKeys.isActivated].boolValue
    subscription = Subscription(json: json[SerializationKeys.subscription])
    surName = json[SerializationKeys.surName].string
    natId = json[SerializationKeys.natId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = noLegalAdvices { dictionary[SerializationKeys.noLegalAdvices] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = nationalityCountryNameAr { dictionary[SerializationKeys.nationalityCountryNameAr] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = fatherName { dictionary[SerializationKeys.fatherName] = value }
    if let value = grandName { dictionary[SerializationKeys.grandName] = value }
    if let value = nationalityCountryId { dictionary[SerializationKeys.nationalityCountryId] = value }
    if let value = natIdExpDate { dictionary[SerializationKeys.natIdExpDate] = value }
    if let value = mobile { dictionary[SerializationKeys.mobile] = value }
    if let value = lastDelegateRequestId { dictionary[SerializationKeys.lastDelegateRequestId] = value }
    if let value = noLawyerRequests { dictionary[SerializationKeys.noLawyerRequests] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = nationalityCountryNameEn { dictionary[SerializationKeys.nationalityCountryNameEn] = value }
    dictionary[SerializationKeys.isActivated] = isActivated
    if let value = subscription { dictionary[SerializationKeys.subscription] = value.dictionaryRepresentation() }
    if let value = surName { dictionary[SerializationKeys.surName] = value }
    if let value = natId { dictionary[SerializationKeys.natId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.noLegalAdvices = aDecoder.decodeObject(forKey: SerializationKeys.noLegalAdvices) as? Int
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.nationalityCountryNameAr = aDecoder.decodeObject(forKey: SerializationKeys.nationalityCountryNameAr) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? String
    self.fatherName = aDecoder.decodeObject(forKey: SerializationKeys.fatherName) as? String
    self.grandName = aDecoder.decodeObject(forKey: SerializationKeys.grandName) as? String
    self.nationalityCountryId = aDecoder.decodeObject(forKey: SerializationKeys.nationalityCountryId) as? Int
    self.natIdExpDate = aDecoder.decodeObject(forKey: SerializationKeys.natIdExpDate) as? String
    self.mobile = aDecoder.decodeObject(forKey: SerializationKeys.mobile) as? String
    self.lastDelegateRequestId = aDecoder.decodeObject(forKey: SerializationKeys.lastDelegateRequestId) as? Int
    self.noLawyerRequests = aDecoder.decodeObject(forKey: SerializationKeys.noLawyerRequests) as? Int
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.nationalityCountryNameEn = aDecoder.decodeObject(forKey: SerializationKeys.nationalityCountryNameEn) as? String
    self.isActivated = aDecoder.decodeBool(forKey: SerializationKeys.isActivated)
    self.subscription = aDecoder.decodeObject(forKey: SerializationKeys.subscription) as? Subscription
    self.surName = aDecoder.decodeObject(forKey: SerializationKeys.surName) as? String
    self.natId = aDecoder.decodeObject(forKey: SerializationKeys.natId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(noLegalAdvices, forKey: SerializationKeys.noLegalAdvices)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(nationalityCountryNameAr, forKey: SerializationKeys.nationalityCountryNameAr)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(fatherName, forKey: SerializationKeys.fatherName)
    aCoder.encode(grandName, forKey: SerializationKeys.grandName)
    aCoder.encode(nationalityCountryId, forKey: SerializationKeys.nationalityCountryId)
    aCoder.encode(natIdExpDate, forKey: SerializationKeys.natIdExpDate)
    aCoder.encode(mobile, forKey: SerializationKeys.mobile)
    aCoder.encode(lastDelegateRequestId, forKey: SerializationKeys.lastDelegateRequestId)
    aCoder.encode(noLawyerRequests, forKey: SerializationKeys.noLawyerRequests)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(nationalityCountryNameEn, forKey: SerializationKeys.nationalityCountryNameEn)
    aCoder.encode(isActivated, forKey: SerializationKeys.isActivated)
    aCoder.encode(subscription, forKey: SerializationKeys.subscription)
    aCoder.encode(surName, forKey: SerializationKeys.surName)
    aCoder.encode(natId, forKey: SerializationKeys.natId)
  }

}
