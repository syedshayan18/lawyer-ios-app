//
//  User.swift
//
//  Created by Invision on 09/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let result = "result"
    static let customer = "customer"
  }

  // MARK: Properties
  public var result: Int?
  public var customer: Customer?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    result = json[SerializationKeys.result].int
    customer = Customer(json: json[SerializationKeys.customer])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = result { dictionary[SerializationKeys.result] = value }
    if let value = customer { dictionary[SerializationKeys.customer] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.result = aDecoder.decodeObject(forKey: SerializationKeys.result) as? Int
    self.customer = aDecoder.decodeObject(forKey: SerializationKeys.customer) as? Customer
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(result, forKey: SerializationKeys.result)
    aCoder.encode(customer, forKey: SerializationKeys.customer)
  }

}
