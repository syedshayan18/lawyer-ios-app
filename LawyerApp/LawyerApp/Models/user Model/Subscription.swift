//
//  Subscription.swift
//
//  Created by Invision on 09/03/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Subscription: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let contractNo = "ContractNo"
    static let cityId = "CityId"
    static let electronicContractNo = "ElectronicContractNo"
    static let delegateId = "DelegateId"
    static let date = "Date"
  }

  // MARK: Properties
  public var contractNo: String?
  public var cityId: Int?
  public var electronicContractNo: Int?
  public var delegateId: Int?
  public var date: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    contractNo = json[SerializationKeys.contractNo].string
    cityId = json[SerializationKeys.cityId].int
    electronicContractNo = json[SerializationKeys.electronicContractNo].int
    delegateId = json[SerializationKeys.delegateId].int
    date = json[SerializationKeys.date].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = contractNo { dictionary[SerializationKeys.contractNo] = value }
    if let value = cityId { dictionary[SerializationKeys.cityId] = value }
    if let value = electronicContractNo { dictionary[SerializationKeys.electronicContractNo] = value }
    if let value = delegateId { dictionary[SerializationKeys.delegateId] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.contractNo = aDecoder.decodeObject(forKey: SerializationKeys.contractNo) as? String
    self.cityId = aDecoder.decodeObject(forKey: SerializationKeys.cityId) as? Int
    self.electronicContractNo = aDecoder.decodeObject(forKey: SerializationKeys.electronicContractNo) as? Int
    self.delegateId = aDecoder.decodeObject(forKey: SerializationKeys.delegateId) as? Int
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(contractNo, forKey: SerializationKeys.contractNo)
    aCoder.encode(cityId, forKey: SerializationKeys.cityId)
    aCoder.encode(electronicContractNo, forKey: SerializationKeys.electronicContractNo)
    aCoder.encode(delegateId, forKey: SerializationKeys.delegateId)
    aCoder.encode(date, forKey: SerializationKeys.date)
  }

}
