//
//  CurrentUser.swift
//  Score My Ice
//
//  Created by Invision on 15/01/2018.
//  Copyright © 2018 invision. All rights reserved.
//

import Foundation

class CurrentUser {
    
    static var customer : Customer?
    static var subscription : Subscription?

    static var customerId : Int?
    static var tokenType : String?
    static var profileUrl : String?
    private init(){
        
    }
    
    
}
