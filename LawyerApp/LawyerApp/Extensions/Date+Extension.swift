//
//  Date+Extension.swift
//  LawyerApp
//
//  Created by Invision on 04/03/2018.
//  Copyright © 2018 invision. All rights reserved.
//
import Foundation
extension DateFormatter {
    func dateformatter(fromSwapiString dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10T16:44:31.486000Z"
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        self.timeZone = TimeZone(abbreviation: "UTC")
        self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
}
