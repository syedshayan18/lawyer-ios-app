//
//  UnderlinedTextField.swift
//  ViewTest
//
//  Created by Invision on 03/08/2017.
//  Copyright © 2017 Invision. All rights reserved.
//

import UIKit

@IBDesignable
class UnderlinedTextField: UITextField {
    
    @IBInspectable var lineWidth: CGFloat = 1.0
    @IBInspectable var lineColor: UIColor = UIColor.blue
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGray
    @IBInspectable var leftIcon:UIImage?
    @IBInspectable var rightIcon:UIImage?
    @IBInspectable var leftIndentation:CGFloat=0
    @IBInspectable var rightIndentation:CGFloat=0
    
    override func draw(_ rect: CGRect) {
        
        self.drawUnderline(lineColor: lineColor, lineWidth: lineWidth)
        //
        self.setAttributesToPlaceholder(textColor: self.placeholderColor)
        self.showLeftIcon()
        self.showRightIcon()
    }
    
    
    //Adds indentation to the Textfield
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: leftIndentation, y: bounds.origin.y, width: bounds.width-rightIndentation, height: bounds.height)
        
    }
    
    //Adds indentation to the editing rect of TextField
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: leftIndentation, y: bounds.origin.y, width: bounds.width-rightIndentation, height: bounds.height)
        
    }
    
    
    /**
     It sets attributes to placeholder.
     
     This method accepts a UIColor, creates an attributed string and assigns it to placeholder.
     
     - parameter textColor: The text color of the placeholder.
     
     */
    
    private func setAttributesToPlaceholder(textColor:UIColor){
        if(self.attributedPlaceholder?.string != nil){
            let placholderString = NSMutableAttributedString(string: self.attributedPlaceholder!.string, attributes: [NSAttributedStringKey.foregroundColor:textColor])
            self.attributedPlaceholder = placholderString
        }
    }
    
    /**
     Draws a line under the textfield of the specified color and width.
     
     This method creates a CALayer and adds it to textfield as a sublayer.
     
     - parameter lineColor: The color of underline.
     
     - parameter lineWidth: The width of underline.
     
     */
    
    private func drawUnderline(lineColor:UIColor,lineWidth:CGFloat){
        
        let underline = CALayer()
        underline.borderColor = lineColor.cgColor
        underline.frame = CGRect(x: 0, y: self.frame.size.height - lineWidth, width:  self.frame.size.width, height: self.frame.size.height)
        underline.borderWidth = lineWidth
        self.layer.addSublayer(underline)
        self.layer.masksToBounds = true
        
    }
    
    /**
     Show icon on the left side of TextField if icon is not nil.
     
     This method creates a UIImageView and adds it to leftView of TextField.
     
     */
    
    func showLeftIcon(){
        if(leftIcon != nil){
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            self.leftView = imageView
            self.leftViewMode = .always
            imageView.contentMode=UIViewContentMode.scaleAspectFit
            imageView.image = leftIcon?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            self.addSubview(imageView)
        }
    }
    
    /**
     
     It adds a UIButton to the textfield and assigns it to the rightView of TextField.
     
     */
    
    func showRightIcon(){
        let errorButton = UIButton(type: .custom)
        errorButton.setImage(rightIcon, for: .normal)
        errorButton.frame = CGRect(x: 0, y: 0, width: 20, height: 40)
        self.rightView = errorButton
        self.rightViewMode = .always
    }
    
    
    
}

