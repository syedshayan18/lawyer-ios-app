//
//  Extension+AlertView.swift
//  Score My Ice
//
//  Created by Invision on 28/12/2017.
//  Copyright © 2017 invision. All rights reserved.
//

import UIKit

extension UIViewController {
    
     func showAlert (title : String? = "Error", message : String?,action1:(title:String, completion:()->Void)? = nil,action2:(title:String,completion: ()->Void)? = nil,action3:(title:String,completion: ()->Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let action1 = UIAlertAction(title: action1?.title, style: .default) { (action) in
            
            if let action = action1 {
                action.completion()
               
            }
            
        }
        alert.addAction(action1)
        
        let action2 = UIAlertAction(title: action2?.title, style: .default){ (action) in
            
            
            if let action = action2 {
                action.completion()
            }
        }
        alert.addAction(action2)
        
        let action3 = UIAlertAction(title: action3?.title, style: .default){ (action) in
            
            
            if let cancelAction = action3 {
                cancelAction.completion()
            }
        }
  
        alert.addAction(action3)
        present(alert, animated: true, completion: nil)
        
    }
    
    func showErrorAlert(title: String? = nil, message: String?) {
        
        let errorAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            
        }
        errorAlert.addAction(okAction)
        present(errorAlert, animated: true, completion: nil)
        
        
     
    }
    
    

    
    

}
